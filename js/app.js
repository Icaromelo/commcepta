// Request json local
function dados(callback) {
    const dados = new XMLHttpRequest();
    dados.overrideMimeType("application/json");
    dados.open('GET', 'dados.json', true); //
    dados.onreadystatechange = function () {
          if (dados.readyState == 4 && dados.status == "200") {
            //
            callback(dados.responseText);
          }
    };
    dados.send(null);
 }

dados(function(response) {
  // Parse JSON string into object
    const dados = JSON.parse(response);
    dados.forEach(function (person) {
    var foto = "/assets/images/" + person.foto;
    var codeBlock = '<div class="cell small-12 medium-6 large-4">' +
                    '<article class="person block-user">' +
                      '<a class="person-item">' +
                        '<div class="person-box grid-x align-middle">' +
                          '<figure style="background-image: url(\''+ foto +'\')" class="cell person-avatar">' +
                            '<span class="avatar-number">' + person.id + '</span>' +
                          '</figure>' +
                          '<div class="person-info cell auto">' +
                            '<legend class="name"><strong>' + person.nome + '</strong></legend>' +
                            '<small class="cargo">' + person.cargo + '</small>' +
                            '<small class="idade hidden">' + person.idade + '</small>' +
                          '</div>' +
                        '</div>' +
                      '</a>' +
                     '</article>' +
                    '</div>';
    $("#person-list").append(codeBlock);
  });

  // Toggle seletores dos blocos e selecão
  $('.block-user').on( "click", function() {
    $(this).addClass('selected');
    $('.block-user').not(this).removeClass("selected");

    const personSelected = $(this);
    const featuredPerson = $('#featured-person');

    let featuredPersonAvatar = featuredPerson.find('.avatar figure');
    let featuredPersonName = featuredPerson.find('.name strong');
    let featuredPersonCargo = featuredPerson.find('.cargo strong');
    let featuredPersonIdade = featuredPerson.find('.idade strong');

    const avatarURL = personSelected.find('.person-avatar').css('background-image');
    console.log(avatarURL);

    featuredPersonAvatar.css('background-image', avatarURL);
    featuredPersonName.html(personSelected.find('.name strong').text());
    featuredPersonCargo.html(personSelected.find('.cargo').text());
    featuredPersonIdade.html(personSelected.find('.idade').text());

    // let teste = personSelected.find('.name strong').text();

    // featuredPersonName = personSelected.find('.name strong').text();
    // featuredPersonCargo = personSelected.find('.name strong').text();
    // featuredPersonIdade = personSelected.find('.name strong').text();

    // console.log($(this).find('.name strong').text());
  });
 });
